package hr.fer.ruazosa.lecture5demo2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import hr.fer.ruazosa.lecture5demo2.databinding.ActivityMainBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.startLongRunningOperationButton.setOnClickListener {
            lifecycleScope.launch(Dispatchers.IO) {
                for (i in 1..10) {
                    Log.d("Counter", binding.toString()+ " " + i)
                    delay(1000)
                    launch(Dispatchers.Main) {
                        binding.operationStatusTextView.text = i.toString()
                    }
                }
            }
        }
        binding.sayHelloButton.setOnClickListener {
            val toastMessage = Toast.makeText(applicationContext, "Hello world", Toast.LENGTH_LONG)
            toastMessage.show()

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        val toastMessage = Toast.makeText(applicationContext, "Activity about to be destroyed", Toast.LENGTH_LONG)
        toastMessage.show()
    }
}